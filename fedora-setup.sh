#!/usr/bin/bash

# Set hostname
sudo hostnamectl set-hostname --static "macro"

# Install Google Chrome / Steam / Grafiktreiber repo
sudo dnf install fedora-workstation-repositories -y

# Install Rpmfusion repo
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y

# Update cache for package installs
sudo dnf makecache

# Install Dropbox
sudo dnf install https://www.dropbox.com/download?dl=packages/fedora/nautilus-dropbox-2018.11.28-1.fedora.x86_64.rpm -y

# Install TeamViewer
sudo dnf install https://download.teamviewer.com/download/linux/teamviewer.x86_64.rpm -y

# grab all packages to install from repos
sudo dnf install $(cat fedora.repopackages) -y

# grab all packages to install from flatpak
sudo flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub $(cat fedora.flatpackages) -y

# enable zsh
chsh -s $(which zsh)

#Disable Wayland and use Xorg
#sudo sed -i '/WaylandEnable/s/^#//g' /etc/gdm/custom.conf

# setup flutter 
mkdir -p $HOME/Dev
cd $HOME/Dev
wget https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.0.0-stable.tar.xz
tar xf flutter_linux_v1.0.0-stable.tar.xz
echo export PATH=$HOME/Dev/flutter/bin:$PATH >> ~/.bashrc
rm flutter_linux_v1.0.0-stable.tar.xz

# setup Android Home
mkdir -p $HOME/Dev/Android/Sdk
echo export ANDROID_HOME=$HOME/Dev/Android/Sdk >> ~/.bashrc
